﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour
{
    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(Play);
    }

    void Play()
    {
        if (AudioManager.instance != null)
        {
            AudioManager.instance.PlayButtonTapSfx();
        }
    }
}