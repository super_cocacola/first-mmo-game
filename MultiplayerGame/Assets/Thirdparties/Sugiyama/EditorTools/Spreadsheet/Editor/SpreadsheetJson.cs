﻿// @version 1.3
// @author unchi


using UnityEngine;
using UnityEditor;

using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace Su {
	
	public class SpreadsheetJson : EditorWindow {
		
        public void Setup () {

			string path = SearchFilePath (_EXEC);

			if (path == null) {
				throw new System.Exception("[" + _EXEC + "] not found");
			}
			
            #if UNITY_EDITOR_WIN
            path = "\"" + path;
            path = path + "\"";

            _toolPath = path;
            #elif UNITY_EDITOR_OSX
            path = "'" + path;
            path = path + "'";

            _toolPath = path;
            #endif
		}

        [MenuItem(_APP_NAME)]
		private static void Init () {
			
			SpreadsheetJson self = EditorWindow.GetWindow (typeof(SpreadsheetJson)) as SpreadsheetJson;

            self.Setup();
			
			self.LoadData();
		}
		
		private void OnGUI () {
			
			_scrollPosition = EditorGUILayout.BeginScrollView (_scrollPosition, GUI.skin.scrollView);
			
			GuiGoogleSettings();
			GuiLogin ();
			GuiOutput ();
			
			GuiExec();
			
			if (GUI.changed) {
				SaveData();
			}
			
			EditorGUILayout.EndScrollView();
		}
		
		// private:
		
		private string ExecRefreshToken (string code) {
			
			List<string> args = new List<string> ();
            args.Add ("-c");
			args.Add (code);
			
			string command = string.Join (" ", args.ToArray());
			string s = RunWait (command);
			
			return s;
		}
		
		private void ExecGenerate (string refreshToken, string spreadsheetTitle, string outputDir) {
			
			List<string> args = new List<string> ();
            args.Add ("-r");
			args.Add (refreshToken);
			args.Add ("-t");
			args.Add (spreadsheetTitle);

			string command = string.Join (" ", args.ToArray());
			string s = RunWait (command);

			StreamReader sr = new StreamReader (s, Encoding.UTF8);
			string line = sr.ReadToEnd();
			JObject jObject = JsonConvert.DeserializeObject(line) as JObject; 
			
			if (jObject == null) {
				EditorUtility.DisplayDialog("Error", "invalid code.", "OK");
				_code = null;
				return;
			}
			
			IEnumerator<KeyValuePair<string, JToken>> e = jObject.GetEnumerator ();
			
			while (e.MoveNext()) {
				string k = e.Current.Key;
				string v = (string)e.Current.Value;
				
				if (!outputDir.EndsWith("/")) {
					outputDir += "/";
				}
				
				Directory.CreateDirectory (outputDir);
				
				string path = outputDir + k + ".json.txt" ;
				
				StreamWriter w = new StreamWriter (path, false, System.Text.Encoding.UTF8);
				w.Write (v);
				w.Close();
			}
			
            Debug.Log("Done!");
		}

		public string SearchFilePath(string fileName)
		{
			FileInfo info = SearchFile(fileName);
			if (info != null)
			{
				return info.FullName;
			}
			return null;
		}
		
		public FileInfo SearchFile(string fileName)
		{
			string path = Application.dataPath;
			DirectoryInfo dir = new DirectoryInfo (path);
			List<FileInfo> lst = DirSearch (dir, fileName);
			
			if (lst.Count >= 1)
				return lst [0];
			
			return null;
		}
		
		private List<FileInfo> DirSearch(DirectoryInfo d, string searchFor)
		{
			List<FileInfo> founditems = new List<FileInfo>(d.GetFiles (searchFor));
			DirectoryInfo[] dis = d.GetDirectories();
			foreach (DirectoryInfo di in dis)
				founditems.AddRange(DirSearch(di, searchFor));
			
			return (founditems);
		}

		private string RunWait (string command) {
			
			System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();

#if UNITY_EDITOR_WIN
			psi.FileName = _toolPath;
#elif UNITY_EDITOR_OSX
			command = _toolPath + " " + command;
            psi.FileName = "/Library/Frameworks/Mono.framework/Versions/Current/Commands/mono";
#endif

            psi.UseShellExecute = false; 
			psi.RedirectStandardOutput = true;
			psi.Arguments = command;

            System.Diagnostics.Process p;

            try
            {
                p = System.Diagnostics.Process.Start(psi);

                string strOutput = p.StandardOutput.ReadToEnd();

				p.WaitForExit();

                return strOutput;
            }
            catch (System.Exception e)
			{
				Debug.Log (e.Message);
                return null;
            }
		}
		
		private void ExecLogin () {
			
			List<string> args = new List<string> ();
            args.Add ("-g");

			string command = string.Join (" ", args.ToArray());
			string s = RunWait (command);
			Debug.Log (s);
			System.Diagnostics.Process.Start(s); 
		}
		
		private void Exec () {
			
			if (string.IsNullOrEmpty(_refreshToken)) {
				
				if (string.IsNullOrEmpty(_code)) {
					UnityEngine.Debug.Log("invalid code !");
					return;
				}
				
				_refreshToken = ExecRefreshToken (_code);
			}

			ExecGenerate(_refreshToken, _spreadsheetTitle, _outputDirectory);
			AssetDatabase.Refresh ();
			
		}
		
		private void GuiGoogleSettings () {
			
			GUILayout.Label ("Google Oauth2", EditorStyles.boldLabel);
			
			GUIStyle styleNone = new GUIStyle();
			styleNone.padding = new RectOffset (14, 0, 0, 0);
			
			GUILayout.BeginVertical (styleNone, new GUILayoutOption[]{});
			
			_toolPath = EditorGUILayout.TextField ("Exe Path:", _toolPath);
			_code = EditorGUILayout.TextField ("Code:", _code);	
			EditorGUILayout.LabelField ("RefreshToken:", _refreshToken);	
			
			GUILayout.EndVertical();
		}
		
		private void GuiOutput() {
			
			GUILayout.Label ("Spreadsheet Settings", EditorStyles.boldLabel);
			
			GUIStyle styleNone = new GUIStyle();
			styleNone.padding = new RectOffset (14, 0, 0, 0);
			
			
			GUILayout.BeginVertical (styleNone, new GUILayoutOption[]{});
			
			_spreadsheetTitle = EditorGUILayout.TextField ("Spreadsheet Title:", _spreadsheetTitle);
			_outputDirectory = EditorGUILayout.TextField ("Output Directory: ", _outputDirectory);
			
			GUILayout.EndVertical();
		}

		private void GuiLogin (){
			
			GUIStyle styleNone = new GUIStyle();
			styleNone.padding = new RectOffset (80, 80, 10, 10);
			
			GUILayout.BeginVertical (styleNone, new GUILayoutOption[]{});
		
			if (string.IsNullOrEmpty (_code)) {
				
				bool on = GUILayout.Button ("Login");
				if (on) {
					ExecLogin();
				}

			} else {
				
				bool on = GUILayout.Button ("Logout");
				if (on) {
					_refreshToken = null;
					_code = null;
				}
			}
			
			GUILayout.EndVertical();
		}
		
		private void GuiExec () {
			if (string.IsNullOrEmpty (_code)) return;
			
			GUIStyle styleNone = new GUIStyle();
			styleNone.padding = new RectOffset (80, 80, 10, 10);
			
			GUILayout.BeginVertical (styleNone, new GUILayoutOption[]{});
			
			bool on = GUILayout.Button ("To JSON");
			
			GUILayout.EndVertical();
			
			if (on) {
				Exec();
			}
			
		}
		
		private void SaveData () {
//			EditorPrefs.SetString (_APP_NAME + "_toolPath", _toolPath);
			EditorPrefs.SetString (_APP_NAME + "_code", _code);
			EditorPrefs.SetString (_APP_NAME + "_refreshToken", _refreshToken);
			EditorPrefs.SetString (_APP_NAME + "_outputDirectory", _outputDirectory);	
			EditorPrefs.SetString (_APP_NAME + "_spreadsheetTitle", _spreadsheetTitle);		
		}
		
		private void LoadData () {
//			_toolPath = EditorPrefs.GetString (_APP_NAME + "_toolPath", _toolPath);
			_code = EditorPrefs.GetString (_APP_NAME + "_code", _code);
			_refreshToken = EditorPrefs.GetString (_APP_NAME + "_refreshToken", _refreshToken);
			_outputDirectory = EditorPrefs.GetString (_APP_NAME + "_outputDirectory", _outputDirectory);
			_spreadsheetTitle = EditorPrefs.GetString (_APP_NAME + "_spreadsheetTitle", _spreadsheetTitle);
		}
		
		private enum Type {
			DEFAULT = 0,
			STRING = DEFAULT,
			INT,
			BOOL,
			FLOAT,
			ARRAY_STRING,
			ARRAY_INT,
			ARRAY_BOOL,
			ARRAY_FLOAT,
			
			COMMENT,
		};
		
		
		private const string _APP_NAME = "Sugiyama/Spreadsheet/Download Json";
		
		private readonly string _EXEC = "Spreadsheet2Json.exe";
		
		
		[SerializeField]
		private string _toolPath;
		[SerializeField]
		private string _code;
		[SerializeField]
		private string _refreshToken;
		[SerializeField]
		private string _spreadsheetTitle;
		
		
		[SerializeField]
		private string _outputDirectory = "./Assets/Datas";
		
		private Vector2 _scrollPosition;
	}
	
} // end of namespace