﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MgAnimationPScaleLoop : MgAnimation
{
    float timerAnimate;
    bool isAnimate;
    RectTransform rectTransform;
    public override void Show()
    {
        rectTransform = GetComponent<RectTransform>();
        rectTransform.localScale = Vector3.one;
        isAnimate = true;
        //LoopAnim();
    }

    public override void Hide(bool immediately = false)
    {
        base.Hide(immediately);
        isAnimate = false;
    }

    void LoopAnim()
    {
        rectTransform.DOPunchScale(Vector3.one * .2f, MgAnimation.ANIM_DUR * 2f).OnComplete(() => { LoopAnim(); });
    }

    private void Update()
    {
        if (isAnimate)
        {
            timerAnimate += Time.deltaTime;
            if (timerAnimate >= MgAnimation.ANIM_DUR * 2f)
            {
                timerAnimate = 0f;
                rectTransform.DOPunchScale(Vector3.one * .25f, MgAnimation.ANIM_DUR * 2f, 0, 0f).SetEase(Ease.Linear);
            }
        }
    }
}
