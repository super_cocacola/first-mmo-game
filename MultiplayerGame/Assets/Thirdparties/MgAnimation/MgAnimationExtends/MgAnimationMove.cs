﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MgAnimationMove : MgAnimation
{
    [SerializeField] Vector2 m_Start;
    [SerializeField] Vector2 m_End;
    [SerializeField] float animDuration = MgAnimation.ANIM_DUR;
    [SerializeField] Ease showEase = Ease.OutBack;
    [SerializeField] Ease hideEase = Ease.InBack;
    [SerializeField] Ease moveEase = Ease.OutQuad;


    public override void Show()
    {
        var rt = GetComponent<RectTransform>();
        rt.DOKill();

        if (gameObject.activeInHierarchy)
        {
            rt.DOAnchorPos(m_End, animDuration).SetEase(showEase);
        }
        else
        {
            rt.anchoredPosition = m_End;
        }
    }

    public override void Hide(bool immediately = false)
    {
        var rt = GetComponent<RectTransform>();
        rt.DOKill();

        if (immediately)
        {
            rt.anchoredPosition = m_Start;
        }
        else
        {
            rt.DOAnchorPos(m_Start, animDuration).SetEase(hideEase);
        }
    }

    public void Move()
    {
        var rt = GetComponent<RectTransform>();
        rt.DOAnchorPos(m_End, animDuration).SetEase(moveEase);
    }

    public void SetStart(Vector3 vector)
    {
        m_Start = vector;
    }

    public void SetEnd(Vector3 vector)
    {
        m_End = vector;
    }
}
