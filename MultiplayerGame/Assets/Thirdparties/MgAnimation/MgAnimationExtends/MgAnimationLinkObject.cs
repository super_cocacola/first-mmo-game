﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MgAnimationLinkObject : MgAnimation
{
    [SerializeField] MgObject m_Link;
    [SerializeField] float delay;
    [SerializeField] bool delayHide = true;

    public override void Show()
    {
        StopAllCoroutines();
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(CoShow());
        }
        else
        {
            m_Link.Show();
        }
    }

    IEnumerator CoShow()
    {
        yield return new WaitForSeconds(delay);
        m_Link.Show();
    }

    public override void Hide(bool immediately = false)
    {
        StopAllCoroutines();
        if (immediately)
        {
            m_Link.Hide(immediately);
        }
        else
        {
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(CoHide());
            }
            else
            {
                m_Link.Hide();
            }
        }
    }

    IEnumerator CoHide()
    {
        if (delayHide)
        {
            yield return new WaitForSeconds(delay);
        }
        m_Link.Hide();
    }
}
