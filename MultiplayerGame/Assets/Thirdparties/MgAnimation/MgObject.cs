﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MgObject : MonoBehaviour
{
    MgAnimation[] m_Anims;
    Graphic m_ButtonTargetGraphic;
    bool canAnimate = true;

    protected virtual void Awake()
    {
        Hide(true);
    }

    protected virtual void OnDisable()
    {
        Hide(true);
    }

    protected void FindAnims()
    {
        if (m_Anims == null)
        {
            m_Anims = GetComponents<MgAnimation>();
        }
    }

    protected void FindButtonTargetGraphic()
    {
        if (m_ButtonTargetGraphic == null)
        {
            var button = GetComponent<Button>();
            if (button != null)
            {
                m_ButtonTargetGraphic = button.targetGraphic;
            }
        }
    }

    public virtual void Show()
    {
        FindAnims();
        FindButtonTargetGraphic();

        for (int i = 0; i < m_Anims.Length; i++)
        {
            m_Anims[i].Show();
        }

        if (m_ButtonTargetGraphic != null)
        {
            m_ButtonTargetGraphic.raycastTarget = true;
        }
    }

    public void Hide(bool immediately = false)
    {
        FindAnims();
        FindButtonTargetGraphic();

        for (int i = 0; i < m_Anims.Length; i++)
        {
            m_Anims[i].Hide(immediately);
        }

        if (m_ButtonTargetGraphic != null)
        {
            m_ButtonTargetGraphic.raycastTarget = false;
        }
    }

    public void Move()
    {
        var anim = GetComponent<MgAnimationMove>();
        anim.Move();
    }

    public void Shake()
    {
        if (canAnimate)
        {
            canAnimate = false;
            var rt = GetComponent<RectTransform>();
            rt.DOShakeAnchorPos(MgAnimation.ANIM_DUR, 10, 10, 40).SetEase(Ease.OutBack).OnComplete(() =>
            {
                canAnimate = true;
            });
        }
    }

    public void PunchScale()
    {
        if (canAnimate)
        {
            canAnimate = false;
            var rt = GetComponent<RectTransform>();
            rt.DOPunchScale(Vector3.one * .2f, MgAnimation.ANIM_DUR).OnComplete(() => { canAnimate = true; });
        }
    }
}
