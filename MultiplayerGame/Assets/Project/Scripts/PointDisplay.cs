﻿using TMPro;
using UnityEngine;

public class PointDisplay : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI text;
    [SerializeField] PointScript pointScript;
    void ChangePoint()
    {
        text.text = pointScript.point.ToString();
    }

    void Update()
    {
        ChangePoint();
    }
}
