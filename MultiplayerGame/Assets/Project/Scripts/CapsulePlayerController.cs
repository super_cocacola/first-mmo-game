﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Mirror.Examples.Chat;

public class CapsulePlayerController : NetworkBehaviour
{
    [SerializeField] GameObject Barrel;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (Input.GetKey(KeyCode.J))
        {
            Barrel.transform.Rotate(0, -1, 0);
        }
        if (Input.GetKey(KeyCode.K))
        {
            Barrel.transform.Rotate(0, 1, 0);
        }
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CmdFire();
        }
    }
    public override void OnStartLocalPlayer()
    {
        MeshRenderer mesh = GetComponent<MeshRenderer>();
        if (mesh) mesh.material.color = Color.blue;
    }
    [Command]
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab 
        var bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        NetworkServer.Spawn(bullet);
        // Add velocity to the bullet 	
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;
        // Destroy the bullet after 2 seconds 
        Destroy(bullet, 2.0f);
    }

}
