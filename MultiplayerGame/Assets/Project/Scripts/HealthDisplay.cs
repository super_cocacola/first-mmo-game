﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    [SerializeField] Image health_bar = null;
    [SerializeField] HealthScript health = null;

    void OnEnable()
    {
        health.EventHealthChanged += Health_EventHealthChanged;
    }

    void OnDisable()
    {
        health.EventHealthChanged -= Health_EventHealthChanged;
    }

    void Health_EventHealthChanged(int currentHealth,int maxHealth)
    {
        health_bar.fillAmount = (float)currentHealth / maxHealth;
    }
}
