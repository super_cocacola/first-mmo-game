﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using UnityEngine.UI;

public class HealthScript : NetworkBehaviour
{
    [Header("Setting")]
    [SerializeField] int maxHealth = 100;
    [SerializeField] int damagePerShoot = 10;
    [SyncVar] int currentHealth;

    public delegate void HealthChangedDelegate(int currentHealth, int maxHealth);
    [SyncEvent]
    public event HealthChangedDelegate EventHealthChanged;

    #region Server
    [Server]
    void SetHealth(int value)
    {
        currentHealth = value;
        EventHealthChanged?.Invoke(currentHealth, maxHealth);
    }
    public override void OnStartServer()
    {
        SetHealth(maxHealth);
    }
    [Command]
    public void CmdDealDamage()
    {
        SetHealth(Mathf.Max(0, currentHealth - damagePerShoot));
    }
    #endregion

    #region client
    [ClientCallback]
    void Update()
    {
        if (!hasAuthority) return;
        if (Input.GetKeyDown(KeyCode.X))
            CmdDealDamage();
    }
    private void OnTriggerEnter(Collider other)
    {
        CmdDealDamage();
    }
    #endregion
}
