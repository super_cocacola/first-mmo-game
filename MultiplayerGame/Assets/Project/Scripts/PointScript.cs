﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PointScript : NetworkBehaviour
{
    [SerializeField] int pointPerTarger = 10;
    [SyncVar]
    public int point;
    [Command]
    void CmdAddPoint()
    {
        point += pointPerTarger;
    }

    void Update()
    {
        if (!hasAuthority) return;
        if (Input.GetKeyDown(KeyCode.X))
            CmdAddPoint();
    }
}
